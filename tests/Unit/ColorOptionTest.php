<?php

namespace Tests\Unit;

use App\ColorOption;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ColorOptionTest extends TestCase
{
    use DatabaseMigrations;

    public function testGetTopColorValueIfThereIsOneWithUniqueNumberOfVotes()
    {
        // ako ima jedna boja sa unikatnim bojem glasova onda vrati njenu vrednost
    }

    function testGetNullIfThereAreMultipleTopColors()
    {
        // ako imaju dve top boje sa istim brojem glasova nemoj da vratis ni jednu
    }

    public function testColorOptionReturnFormattedAllOptions()
    {
        ColorOption::create([
            'group' => 'red',
            'value' => '111',
            'position' => 1,
            'number_of_votes' => 13,
        ]);

        ColorOption::create([
            'group' => 'red',
            'value' => 'aaa',
            'position' => 0,
        ]);

        ColorOption::create([
            'group' => 'red',
            'value' => 'fff',
            'position' => 2,
        ]);

        $options = ColorOption::getAllOptions();

        $this->assertEquals([
            'red' => [
                'group' => 'red',
                'values' => [
                    '2' => [
                        'value' => 'aaa',
                        'number_of_votes' => '0',
                    ],
                    '1' => [
                        'value' => '111',
                        'number_of_votes' => '13',
                    ],
                    '3' => [
                        'value' => 'fff',
                        'number_of_votes' => '0',
                    ],
                ],
            ],
        ], $options);
    }
}
