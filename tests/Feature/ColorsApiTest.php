<?php

namespace Tests\Feature;

use App\ColorOption;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ColorsApiTest extends TestCase
{
    use DatabaseMigrations, InteractsWithExceptionHandling;

    public function testGettingAllColorOptions()
    {
        $this->seedColors();

        $response = $this->get('/api/colors');

        $response->assertStatus(200)
            ->assertExactJson([[
                'red' => [
                    'group' => 'red',
                    'values' => [
                        '1' => [
                            'value' => '111',
                            'number_of_votes' => '0',
                        ],
                        '3' => [
                            'value' => 'aaa',
                            'number_of_votes' => '0',
                        ],
                    ],
                ],
                'blue' => [
                    'group' => 'blue',
                    'values' => [
                        '2' => [
                            'value' => 'fff',
                            'number_of_votes' => '69',
                        ],
                    ],
                ],
            ]]);
    }

    public function testVotingForAColorUpdatesAndReturnsTheVoteCount()
    {
        $this->withoutExceptionHandling();

        ColorOption::create([
            'group' => 'red',
            'value' => '111',
            'number_of_votes' => 99,
        ]);

        $votedColor = ColorOption::find(1);
        $this->assertEquals(99, $votedColor->number_of_votes);

        $response = $this->post('/api/colors/1/vote');

        $response->assertStatus(200)
            ->assertExactJson(['vote_count' => 100]);

        $votedColor = ColorOption::find(1);
        $this->assertEquals(100, $votedColor->number_of_votes);
    }

    public function test404responseForVotingOnNonExistingColor()
    {
        $response = $this->post('/api/colors/1/vote');

        $response->assertStatus(404);
    }

    private function seedColors()
    {
        ColorOption::create([
            'group' => 'red',
            'value' => '111',
            'position' => 0,
        ]);

        ColorOption::create([
            'group' => 'blue',
            'value' => 'fff',
            'position' => 2,
            'number_of_votes' => 69,
        ]);

        ColorOption::create([
            'group' => 'red',
            'value' => 'aaa',
            'position' => 1,
        ]);
    }
}

