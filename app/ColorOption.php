<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColorOption extends Model
{
    protected $fillable = [
        'group', 'value', 'number_of_votes',
    ];

    /**
     * Get all color options and group them by column group
     *
     * @return array
     */
    public static function getAllOptions()
    {
        $allOptions = (new static)->newQuery()->orderBy('position')->get()->toArray();

        return array_reduce($allOptions, function($carry, $option) {
            $carry[$option['group']]['group'] = $option['group'];
            $carry[$option['group']]['values'][$option['id']] = [
                'value' => $option['value'],
                'number_of_votes' => $option['number_of_votes'],
            ];
            return $carry;
        }, []);
    }
}
