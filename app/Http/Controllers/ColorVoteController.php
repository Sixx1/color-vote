<?php

namespace App\Http\Controllers;

use App\ColorOption;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class ColorVoteController extends BaseController
{
    public function index()
    {
        $options = ColorOption::getAllOptions();
        
        return response()->json([
            $options,
        ]);
    }

    public function vote($id)
    {
        // TODO refactor move updating vote count to the model
        $color = ColorOption::findOrFail($id);
        $color->number_of_votes = $color->number_of_votes + 1;
        $color->save();

        return response()->json([
            'vote_count' => $color->number_of_votes,
        ]);
    }
}
