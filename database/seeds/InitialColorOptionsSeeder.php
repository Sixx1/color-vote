<?php

use Illuminate\Database\Seeder;
use App\ColorOption;

class InitialColorOptionsSeeder extends Seeder
{
    /**
     * Run the database seeds to fill in initial color options
     *
     * @return void
     */
    public function run()
    {
        $options = [
            [
                'group' => 'red',
                'value' => 'BE1E2D',
                'position' => 0,
            ],
            [
                'group' => 'red',
                'value' => 'EC1C24',
                'position' => 1,
            ],
            [
                'group' => 'red',
                'value' => 'ED5A74',
                'position' => 2,                
            ],
            [
                'group' => 'green',
                'value' => '00A551',
                'position' => 0,
            ],
            [
                'group' => 'green',
                'value' => '8BC53F',
                'position' => 1,
            ],
            [
                'group' => 'green',
                'value' => '32C6B0',
                'position' => 2,                
            ],
            [
                'group' => 'blue',
                'value' => '1B75BB',
                'position' => 0,
            ],
            [
                'group' => 'blue',
                'value' => '00ADEE',
                'position' => 1,
            ],
            [
                'group' => 'blue',
                'value' => '00BCD4',
                'position' => 2,                
            ],
        ];
        
        foreach($options as $option) {
            ColorOption::firstOrCreate($option);
        }
    }
}
