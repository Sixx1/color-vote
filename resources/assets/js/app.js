require('./bootstrap');

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import VoteWrapper from './components/VoteWrapper';

if (document.getElementById('color-app')) {
    ReactDOM.render(<VoteWrapper />, document.getElementById('color-app'));
}
