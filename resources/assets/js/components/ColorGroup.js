import React, { Component } from 'react';
import ColorBox from './ColorBox';
import ResultBox from './ResultBox';

export default class ColorGroup extends Component {
    constructor(props) {
        super(props);
        
        // This binding is necessary to make `this` work in the callback
        this.voteForColor = this.voteForColor.bind(this);
    }

    voteForColor(id) {
        if (!localStorage.getItem('voted_for_' + this.props.group.group)) {
            localStorage.setItem('voted_for_' + this.props.group.group, id);
            axios.post('/api/colors/' + id + '/vote')
                .then((response) => {
                    this.props.group.values[id]['number_of_votes'] = response.data['vote_count'];
                    this.forceUpdate();
                });
        }
    }

    render() {
        const votedForId = localStorage.getItem('voted_for_' + this.props.group.group);
        if (!votedForId) {
            return (
                <div className="col-md-12 color-group">
                    <p>Vote for {this.props.group.group}</p>
                    {Object.keys(this.props.group.values).map((id) => {
                        return <ColorBox key={id} id={id} box={this.props.group.values[id]} voteEvent={this.voteForColor} />
                    })}
                </div>
            );
        }

        return (
            <div className="col-md-12 color-group">
                <p>Results for {this.props.group.group}!</p>
                {Object.keys(this.props.group.values).map((id) => {
                    return <ResultBox 
                        key={id} 
                        numberOfVotes={this.props.group.values[id].number_of_votes}
                        userVoted={votedForId === id}
                        value={this.props.group.values[id].value}
                    />
                })};
            </div>
        );
    }
}
