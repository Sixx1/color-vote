import React, { Component } from 'react';

export default class ColorBox extends Component {
    render() {
        return (
            <div 
                onClick={this.props.voteEvent.bind(this, this.props.id)}
                className="color-box"
                style={{backgroundColor: '#' + this.props.box.value }}
            >
            </div>
        );
    }
}
