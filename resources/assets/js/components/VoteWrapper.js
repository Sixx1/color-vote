import React, { Component } from 'react';
import ColorGroup from './ColorGroup';

export default class VoteWrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {ready: false};
        axios.get('/api/colors')
            .then((response) => {
                this.state.data = response.data[0];
                this.setState({'ready': true});
            });
    }

    render() {
        if (this.state.ready) {
            return (
                <div className="container">
                    <div className="row justify-content-center">
                        {Object.keys(this.state.data).map(groupname => {
                            return <ColorGroup key={groupname} group={this.state.data[groupname]} />
                        })}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="container">
                    <h4>Loading...</h4>
                    <img src="/assets/loading.gif" />
                </div>
            )
        }
    }
}
