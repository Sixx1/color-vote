import React, { Component } from 'react';

export default class ColorBox extends Component {
    render() {
        let classes = "result ";
        classes += this.props.numberOfVotes > 0 ? "color-box" : "color-box no-votes";
        classes += this.props.userVoted ? ' user-voted' : '';
        return (
            <div 
                className={classes}
                style={{
                    backgroundColor: '#' + this.props.value, 
                    flexGrow: this.props.numberOfVotes
                }}
            >
                <span className="number-of-votes">{this.props.numberOfVotes}</span>
            </div>
        );
    }
}
