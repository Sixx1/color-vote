<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Color Vote</title>
        <link rel="stylesheet" type="text/css" href="/css/app.css">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    Color Vote
                </div>
                <h4 class="margin-xl-bottom">Click to vote for your favorite shade of colors below!</h4>
                <div id="color-app"></div>
            </div>
        </div>
    </body>
    <script src="/js/app.js"></script>
</html>
